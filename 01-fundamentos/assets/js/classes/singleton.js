class Singleton {

    static instancia;
    nombre = '';

    constructor( nombre = '' ) {
        // Se inicializa el valor de instancia como el objeto y si
        // instancia ya tiene un objeto, se regresa el mismo valor sin
        // modificar sus valores
        if ( !!Singleton.instancia ) {
            console.log(`if: ${Singleton.instancia.nombre}`)
            return Singleton.instancia;
        }
        Singleton.instancia = this;
        this.nombre = nombre;

        return this;
    }
}

const inst1 = new Singleton('name1');
const inst2 = new Singleton('name2');

console.log(`Nombre en la inst1 es: ${ inst1.nombre }`);
console.log(`Nombre en la inst2 es: ${ inst2.nombre }`);