class Persona {

    static _conteo = 0;
    static get conteo() {
        return Persona._conteo + 'instancias';
    }
    static mensaje() {
        console.log(this.nombre);
        console.log('Mensaje estático');
    }

    // comida = '';
    constructor( nombre, codigo, frase ) {
        Persona._conteo++;
        this.nombre = nombre;
        this.codigo = codigo;
        this.frase = frase;
    }

    quienSoy() {
        console.log(`Mi nombre es ${ this.nombre }`);
    }

    /**
     * @param {string} comida
     */
    set setComidaFavorita( comida ) {
        this.comida = comida.toUpperCase();
    }

    get getComidaFavorita() {
        return `La comida favorita de ${ this.nombre } es ${ this.comida }`;
    }

}

const pedro = new Persona( 'Peter', 'A113', 'Frase' );
const pedro2 = new Persona( 'Peter2', 'A1132', 'Frase2' );

console.log(pedro2);
Persona.mensaje();