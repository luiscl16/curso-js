class Rectangulo {
    // Propiedad privada
    #area = 0;
    constructor( base = 0, altura = 0 ) {
        this.base = base;
        this.altura = altura;

        this.#area = base * altura;
    }
}

const rectangulo = new Rectangulo( 5, 8 );
// rectangulo.#area = 100;