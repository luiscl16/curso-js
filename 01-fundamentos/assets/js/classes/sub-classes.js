class Persona {

    static _conteo = 0;
    static get conteo() {
        return Persona._conteo + 'instancias';
    }
    static mensaje() {
        console.log(this.nombre);
        console.log('Mensaje estático');
    }

    // comida = '';
    constructor( nombre, codigo, frase ) {
        Persona._conteo++;
        this.nombre = nombre;
        this.codigo = codigo;
        this.frase = frase;
    }

    quienSoy() {
        console.log(`Mi nombre es ${ this.nombre }`);
    }

    /**
     * @param {string} comida
     */
    set setComidaFavorita( comida ) {
        this.comida = comida.toUpperCase();
    }

    get getComidaFavorita() {
        return `La comida favorita de ${ this.nombre } es ${ this.comida }`;
    }

}

class Dude extends Persona {
    clan = 'sin clan';
    constructor( nombre, codigo, frase ) {
        super( nombre, codigo, frase );
        this.clan = 'sadsa';
    }

    quienSoy() {
        console.log(`Mi nombre es [2] ${ this.nombre }`);
        super.quienSoy();
    }
}

const peter = new Dude( 'Peter', 'A113', 'Frase' );
peter.quienSoy();