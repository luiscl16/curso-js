function Persona(nombre, edad) {
    console.log('Se ejecutó esta línea');
    this.nombre = nombre;
    this.edad = edad;

    this.imprimir = function() {
        console.log(`Nombre: ${ this.nombre } - edad: ${ this.edad }`);
    }
}

const maria = new Persona('María', 18);
const melisa = new Persona('Melisa', 35);

// console.log(maria);
melisa.imprimir();
maria.imprimir();