/**
 * Lee el valor en puntaje de una carta
 * @param {String} carta
 * @returns {Number} valor de la carta
 */
export const valorCarta = ( carta ) => {

    if ( !carta || carta === '' )
        throw new Error('carta es obligatorio y no debe ser vacía');

    const valor = carta.substring(0, carta.length - 1);
    return ( isNaN( valor ) ) ? 
            ( valor === 'A' ) ? 11 : 10
            : valor * 1;
}