export {crearDeck} from './crear-deck';
export {crearCartaHTML} from './crear-carta-HTML';
export {pedirCarta} from './pedir-carta';
export {turnoComputadora} from './turno-computadora';
export {valorCarta} from './valor-carta';