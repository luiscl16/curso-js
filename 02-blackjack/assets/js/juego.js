const miModulo = (() => {
    'use strict'
    let deck         = [];
    const tipos      = ['C', 'D', 'H', 'S'],
          especiales = ['A', 'J', 'Q', 'K'];
    
    let puntosJugadores = [];
    
    // Referencias del HTML
    const btnPedir             = document.querySelector('#btnPedir'),
          btnDetener           = document.querySelector('#btnDetener'),
          btnNuevo             = document.querySelector('#btnNuevo');

    const divCartasJugadores = document.querySelectorAll('.divCartas'),
          puntosHTML         = document.querySelectorAll('small');

    const inicializarJuego = ( numJugadores = 2 ) => {
        deck = crearDeck();
        puntosJugadores = [];
        for ( let i = 0; i < numJugadores; i++) {
            puntosJugadores.push(0)
        }

        puntosHTML.forEach( elem => elem.innerText = 0 );
        divCartasJugadores.forEach( elem => elem.innerHTML = '' );

        // divCartasJugadores.replaceChildren();
        // divCartasComputadora.replaceChildren();

        btnPedir.disabled = false;
        btnDetener.disabled = false
    }
    
    const crearDeck = () => {
        deck = [];
        for ( let i = 2; i <= 10; i++ ) {
            for (let tipo of tipos) {
                deck.push( i + tipo )
            }
        }
    
        for ( let especial of especiales ) {
            for ( let tipo of tipos ) {
                deck.push( especial + tipo )
            }
        }
    
        return _.shuffle( deck );
    }
    
    const pedirCarta = () => {
        if ( deck.length === 0 ) {
            throw 'No hay cartas en el deck';
        }
        
        return deck.pop();
    }
    
    const valorCarta = ( carta ) => {
        const valor = carta.substring(0, carta.length - 1);
        return ( isNaN( valor ) ) ? 
                ( valor === 'A' ) ? 11 : 10
                : valor * 1;
    }

    // Turno: 0 = primer jugador, último será computadora
    const acumularPuntos = ( carta, turno ) => {
        puntosJugadores[turno] = puntosJugadores[turno] + valorCarta( carta )
        console.log('turno: ', turno);
        puntosHTML[turno].innerText = puntosJugadores[turno];
        return puntosJugadores[turno];
    }

    const crearCarta = ( carta, turno ) => {
        const imagenCarta = document.createElement('img');
        imagenCarta.classList.add('carta')
        imagenCarta.src = `assets/cartas/${ carta }.png`;
        divCartasJugadores[turno].append( imagenCarta );
    }

    const determinarGanador = () => {

        const [ puntosMinimos, puntosComputadora ] = puntosJugadores;

        setTimeout(() => {
            if ( puntosComputadora === puntosMinimos ) {
                alert('Nadie gana');
            } else if ( puntosMinimos > 21 ) {
                alert('Computadora gana');
            } else if ( puntosComputadora > 21 ) {
                alert('Jugador gana');
            } else if ( puntosComputadora > puntosMinimos ) {
                alert('Computadora gana');
            } else {
                alert('Jugador gana');
            }
        }, 200)
    }
    
    const turnoComputadora = ( puntosMinimos ) => {
        let puntosComputadora = 0;
        do {
            const carta = pedirCarta();
            puntosComputadora = acumularPuntos( carta, puntosJugadores.length - 1 )
            crearCarta( carta, puntosJugadores.length - 1 )
    
        } while ( (puntosComputadora < puntosMinimos) && (puntosMinimos <= 21) );
    
        determinarGanador();
    
    }
    
    // Eventos
    btnPedir.addEventListener('click', () => {
        const carta = pedirCarta();
        const puntosJugador = acumularPuntos(carta, 0);

        crearCarta( carta, 0 );
    
        if ( puntosJugador > 21 ) {
            console.warn('Perdiste');
            btnPedir.disabled = true;
            btnDetener.disabled = true;
            turnoComputadora( puntosJugador )
        } else if ( puntosJugador === 21 ) {
            btnPedir.disabled = true;
            btnDetener.disabled = true;
            console.warn('21!');
            turnoComputadora( puntosJugador )
        }
    })
    btnDetener.addEventListener('click', () => {
        btnPedir.disabled = true;
        btnDetener.disabled = true;
        turnoComputadora( puntosJugadores[0] );
    });
    btnNuevo.addEventListener('click', () => {
        inicializarJuego();
    });

    return {
        nuevoJuego: inicializarJuego
    };
})()

